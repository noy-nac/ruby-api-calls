Rails.application.routes.draw do
  
  root 'home#index'

  # used by app1 to send a cat
  post '/pls_send_cat', to: 'home#pls_send_cat'

  # check for a cat from app1
  post '/check_cat', to: 'home#check_cat'
end
