class HomeController < ApplicationController
  
  # Don't use a session token since we allow external use
  # (there are better ways to do this)
  protect_from_forgery with: :null_session

  # global variables have $
  # (class variables have @@)

  $cat_url = ""
  $cat_name = "No cats yet"

  def index
  end

  def check_cat

    #

    @cat_name = $cat_name
    @cat_pic_url = $cat_url

  end

  def pls_send_cat
   
    # Request parameters
    # access using params[:param_name]

    $cat_url = params[:url]
    $cat_name = params[:name]
    
  end

end
