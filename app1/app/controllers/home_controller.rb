

require 'uri'
require 'net/http'
require 'json'


class HomeController < ApplicationController
  def index
  end

  # /cat
  def get_cat
	
	# TODO EXAMPLE RESPONSE

	# acquire cat
	uri = URI("https://api.thecatapi.com/v1/images/search")
	res = Net::HTTP.get(uri)

	val = JSON.parse(res)[0]['url']
	
	@cat_pic_url = val

  end

  # /ship
  def send_cat

	# package cat and deliver to client
	uri = URI("http://localhost:3002/pls_send_cat")

	url_val = params[:cat_url] # -> http://catimgco.com/1
	name_val = params[:cat_name]

	res = Net::HTTP.post_form(uri, 'url' => url_val, 'name' => name_val)

	#
	@msg = 'cat sent' if res.is_a?(Net::HTTPSuccess)


  end

end
