Rails.application.routes.draw do

  root 'home#index'

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # route to get a cat
  post '/cat', to: 'home#get_cat'
  
  # route to send a cat app2
  post '/ship', to: 'home#send_cat'

end
